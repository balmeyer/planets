extends Node3D

class_name TerrainBuilder

@export var noise : Noise
@export var terrain_model = preload("res://terrains/terrain.tscn")
@export var grid_size : Vector2 = Vector2(3, 3)
@export var terrain_size = 50
@export var follow_name = "Vessel"

@onready var terrains : Node3D = $Terrains

var _target: Node3D
var distances : Array[float]
var terrain_update_working = false

# Called when the node enters the scene tree for the first time.
func _ready():
	build_noise()
	build_map()
	build_following()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func follow(node : Node3D):
	_target = node

func update_terrains():
	if _target == null: return
	
	if terrain_update_working:
		return
	
	var ca = terrain_size
	
	const max_division = 10
	
	for c in terrains.get_children():
		var t = c as Node3D
		var dist = t.position.distance_to(_target.position)
		var case_distance = floor(dist / terrain_size)
		var case = clamp(floor(dist / terrain_size), 0, max_division)
		
		if case > 3:continue
		
		var division = 32
		if case <= 4:
			division = 128
		if case <= 2:
			division = 512
		if t.division != division:
			terrain_update_working = true
			t.update_division(division)
			break
			
		
func build_following():
	var node = get_tree().root.find_child(follow_name, true, false)
	follow(node)

func build_noise():
	if noise == null:
		noise.noise_type = FastNoiseLite.TYPE_PERLIN
		noise.fractal_type = FastNoiseLite.FRACTAL_FBM
		noise.fractal_octaves = 8
		noise.fractal_gain = 0.6
		noise.fractal_lacunarity = 1.5

func build_array():
	pass

func build_map():
	# build distance
	distances = []
	
	# for c in get_children(): c.queue_free()
	
	var start_position = Vector3(grid_size.x / 2, 0, grid_size.y / 2) * terrain_size
	for z in range(0, grid_size.y):
		for x in range(0, grid_size.x):
			var instance = terrain_model.instantiate()
			terrains.add_child(instance)
			instance.name = "terrain_" + str(x) + "-" + str(z)
			instance.builder = self
			instance.world_position = Vector2(x * terrain_size, z* terrain_size)
			instance.plane_size = Vector2(terrain_size, terrain_size)
			instance.position = Vector3(x * terrain_size, 0, z* terrain_size) - start_position
			instance.update_division(64)
			instance.updated.connect(_terrain_updated)

func get_height(point: Vector2) -> float:
	return noise.get_noise_2d(point.x / 100, point.y / 100) * 500.0

func _on_timer_timeout():
	update_terrains()

func _terrain_updated(terrain):
	print("updated ", terrain.name)
	terrain_update_working = false
