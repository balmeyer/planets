extends Node3D

signal updated

'''
this is a first iteration with a plane world
'''

@export var mat : Material

@export var plane_size = Vector2(100, 100)

var builder
var division = 4

const max_division = 1024

var world_position = Vector2(0, 0)
var planet_center:Vector3 = Vector3(0, -100, 0)

var array_vertex = PackedVector3Array()
var plane_mesh : MeshInstance3D
var mesh : ArrayMesh
var current_thread:int = -1
var mesh_ready = false

@onready var static_object : StaticBody3D = $StaticBody3D
@onready var collision_shape : CollisionShape3D = $StaticBody3D/CollisionShape3D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _process(delta):
	if mesh_ready:
		mesh_ready = false
		WorkerThreadPool.add_task(commit_surface, true, "commit surface")
		# commit_surface()

	
func get_step() -> int:
	return clamp(division + 1, 1, max_division)
	
func update_division(d:int):
	d = clamp(d, 0 , max_division)
	
	if division != d:
		division = d
		start_build_array()
	
func get_height(v: Vector3) -> float:
	if builder == null: return 0.0
	var v2 = Vector2(v.x, v.z) + world_position
	var n = builder.get_height(v2)
	return n

func start_build_array():
	if current_thread >= 0:
		if not WorkerThreadPool.is_task_completed(current_thread):
			print("thread still active ", current_thread)
			return
	current_thread = WorkerThreadPool.add_task(build_all)

func build_all():
	build_array()
	build_surface()

func build_array():
	division = clamp(division, 0 , max_division)
	var origin = Vector3(plane_size.x / -2, 0, plane_size.y / -2)
	
	# each distance between points
	var step = get_step()
	var offset_x = plane_size.x / step
	var offset_z = plane_size.y / step
	
	var array = PackedVector3Array()
	
	# construct vertex
	var current_point = origin
	for z in range(0, step + 1):
		for x in range (0, step + 1):
			current_point.y = get_height(current_point)
			var relative_to_center = current_point
			array.append(relative_to_center)
			current_point.x += offset_x
		# next z
		current_point.z += offset_z
		current_point.x = origin.x
	array_vertex = array

func build_surface():
	# construct face
	print("build surface ", name, " division ", division)
	var surface_tool = SurfaceTool.new()
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface_tool.set_uv(Vector2.ZERO)
	var step = get_step()
	
	var i = 0
	var next_line = step + 1
	
	for z in range(0, step):
		var uv_z = z as float / division as float
		for x in range(0, step + 1):
			if x < step:
				# add vertex
				var v1 = array_vertex[i]
				var v2 = array_vertex[i + 1]
				var v3 = array_vertex[i + next_line]
				var v4 = array_vertex[i + next_line + 1]
				
				# uv
				var uv_x = x as float / division as float

				# triangle 1
				# surface_tool.set_uv(Vector2(uv_x, uv_z))
				surface_tool.add_vertex(v1)
				surface_tool.add_vertex(v2)
				surface_tool.add_vertex(v3)
				# triangle 2
				surface_tool.add_vertex(v2)
				surface_tool.add_vertex(v4)
				surface_tool.add_vertex(v3)
			i += 1
	
	surface_tool.generate_normals()
	
	if plane_mesh == null:
		plane_mesh = MeshInstance3D.new()
		add_child(plane_mesh)
	mesh = surface_tool.commit()
	mesh_ready = true

func commit_surface():
	var t = Time.get_ticks_msec()
	plane_mesh.mesh = mesh
	plane_mesh.mesh.surface_set_material(0, mat)
	if false and division <= 256:
		var shape = mesh.create_trimesh_shape()
		collision_shape.shape = shape
	var done = Time.get_ticks_msec() - t
	print("time ", done)
	updated.emit(self)
