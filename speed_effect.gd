extends Node3D


@onready var vessel : CharacterBody3D
@onready var multi : MultiMeshInstance3D = $MultiMeshInstance3D
var started = false

var next_compute:float = 0
var t:int

# Called when the node enters the scene tree for the first time.
func _ready():
	call_deferred("init_effect")


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if started: 
		if t > 0 and not WorkerThreadPool.is_task_completed(t):
			return
		t = WorkerThreadPool.add_task(compute_effect)


func init_effect():
	vessel = get_tree().root.find_child("Vessel", true, false) as CharacterBody3D
	prints("Vessel", vessel)
	started = true
	
	var vessel_transform = vessel.transform
	
	for i in range(multi.multimesh.instance_count):
		var pos = Transform3D()
		pos = vessel_transform.translated(Vector3(randf() * 100 - 50, randf() * 50 - 25, randf() * 50 - 25))
		multi.multimesh.set_instance_transform(i, pos)
		

func compute_effect():
	if next_compute > Time.get_ticks_msec():
		return

	next_compute = Time.get_ticks_msec() + 500
	
	var vessel_transform = vessel.transform

	var center = vessel.transform.translated(vessel.basis.z * -vessel.velocity.length())
	
	const max_distance = 120 * 120
	var spread = 50
	var double_spread = spread * 2
	
	for i in range(multi.multimesh.instance_count):
		var pos = multi.multimesh.get_instance_transform(i) as Transform3D
		if pos.origin.distance_squared_to(vessel.position) > max_distance:
			pos = center.translated(Vector3(randf() * double_spread - spread, randf() * double_spread - spread, randf() * double_spread - spread))
			multi.multimesh.set_instance_transform(i, pos)
	
