extends Node3D

'''
Action on setting vessel position to (0, 0, 0), and all the world around
'''

var next_recenter:float = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	if next_recenter < Time.get_ticks_msec():
		next_recenter = Time.get_ticks_msec() + 5000
		recenter()
		
		
func recenter():
	var vessel = get_tree().get_first_node_in_group("Vessel") as Node3D
	if vessel.position.length() > 20000:
		var offset = -vessel.position
		for p in get_tree().get_nodes_in_group("Planets"):
			p.recenter(offset)
		vessel.position = Vector3.ZERO
