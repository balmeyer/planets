extends CharacterBody3D


const SPEED = 20.0
const MAX_SPEED = 22.0
const PITCH_SPEED = 0.8

var engine = 0
var orbiting_celestial:Node3D

var up_position:Vector3 = Vector3.ZERO

@onready var pivot : Node3D = $Pivot

var speed_scale:float = 10.0

func _ready():
	add_to_group("Vessel")

func _physics_process(delta):
	update_gravity(delta)
	update_up_position(delta)
	
	var pitch = Input.get_axis("pitch_up", "pitch_down")
	var yaw = Input.get_axis("yaw_left", "yaw_right")
	var roll = Input.get_axis("roll_left", "roll_right")
	var accel = Input.get_axis("engine_down", "engine_up")

	pivot.rotation = lerp(pivot.rotation, Vector3.ZERO, delta * 6)
	
	if accel > 0:
		engine += (1 + engine) * delta
		# print("speed ", SPEED * engine, " km/s")
	elif accel < 0:
		engine = 0
	
	engine = clamp(engine, 0, MAX_SPEED * speed_scale)
	
	if pitch != 0:
		rotate(transform.basis.x, -pitch * delta * PITCH_SPEED)
		pivot.rotate_x(pitch * delta * PITCH_SPEED)
		
	if yaw != 0:
		rotate(transform.basis.y, -yaw * delta * PITCH_SPEED)
		pivot.rotate_y(yaw * delta * PITCH_SPEED)
		
	if roll != 0:
		rotate(transform.basis.z, -roll * delta * PITCH_SPEED)
		pivot.rotate_z(roll * delta * PITCH_SPEED)
	
	var direction = (-transform.basis.z).normalized()
	if engine > 0:
		velocity = direction * SPEED * engine
	else:
		velocity = lerp(velocity, Vector3.ZERO, delta)

	move_and_slide()

func update_gravity(delta:float):
	if orbiting_celestial != null:
		up_position = orbiting_celestial.position.direction_to(self.position)
	else:
		up_position = Vector3.ZERO

func update_up_position(delta:float):
	if up_position != Vector3.ZERO:
		var angle = up_position.angle_to(transform.basis.y)
		var v1 = transform.basis.y
		

func remove_gravity_center(planet:Node3D):
	if planet == orbiting_celestial:
		orbiting_celestial = null
		speed_scale = 10.0
		print(" END gravity ", planet)

func set_gravity_center(planet:Node3D):
	print(" gravity ", planet)
	orbiting_celestial = planet
	speed_scale = 1.0
