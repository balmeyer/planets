extends StaticBody3D

class_name GeneratedPlanet

"""
one unit = 1 km
"""

@export var radius : float  = 500.0
@export var segments : int = 16
@export var map_size: Vector2 = Vector2(1024, 512)
@export var noise : FastNoiseLite

@onready var mesh_instance : MeshInstance3D = $PlanetMesh
@onready var cursor : MeshInstance3D = $Cursor
@onready var planet_data : PlanetData = $PlanetData

var absolute_origin : Vector3

var orbit_height : float  = 200.0

var following_name = "Vessel"
var target: Node3D
var orbit_distance:float
var orbiting = false

var _nearest_point:Vector3

# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("Planets")
	init_planet()
	generate_planet()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	update_position_and_scale()
	update_nearest_point()

func recenter(offset:Vector3):
	absolute_origin += offset
	position += offset

func get_mesh():
	return mesh_instance.mesh

func get_nearest_point() -> Vector3:
	return _nearest_point

func get_distance_to_camera() -> float:
	return absolute_origin.distance_to(target.position) - radius

func get_uv_from_point(point:Vector3) -> Vector2:
	var n = point.normalized() * -1.0
	var u = 0.5 + atan2(n.x, n.z) / (2.0 * PI)
	var v = 0.5 + asin(n.y) / PI
	var uv = Vector2(u, v)
	return uv

func get_height(vec:Vector3) -> float:
	var uv = get_uv_from_point(vec)
	var h = planet_data.get_height_from_uv(uv)
	return h
	
func init_planet():
	orbit_height = radius / 5.0
	orbit_distance = radius + orbit_height
	target = get_tree().root.find_child(following_name, true, false) as Node3D
	cursor.position = Vector3(0, radius + 1, 0)

func generate_planet():
	# generate data
	planet_data.compute_data(map_size, noise)
	
	# create sphere
	var mesh = SphereMesh.new()
	mesh.radius = radius
	mesh.height = radius * 2
	mesh.radial_segments = segments
	mesh.rings = segments
	mesh_instance.mesh = mesh
	
	# mat
	var image = planet_data.get_image()
	var texture = ImageTexture.create_from_image(image)
	
	# tmp : place a noise texture instead
	var noise_texture = NoiseTexture2D.new()
	noise_texture.noise = noise
	noise_texture.width = map_size.x
	noise_texture.height = map_size.y
	noise_texture.seamless = true
	noise_texture.invert = true

	# set material with shader
	var mat : ShaderMaterial = ShaderMaterial.new()
	var shader : Shader = load("res://planets/generated_planet.gdshader")
	mat.shader = shader
	mat.set_shader_parameter("color_sea", Color.BLUE)
	mat.set_shader_parameter("color_coast", Color.YELLOW)
	mat.set_shader_parameter("color_plain", Color.DARK_GREEN)
	mat.set_shader_parameter("color_mountain", Color.WHITE_SMOKE)
	mat.set_shader_parameter("planet_texture", noise_texture)
	
	mesh_instance.set_surface_override_material(0, mat)
	
	absolute_origin = self.position


func update_position_and_scale():
	var distance_to_center_origin = target.position.distance_to(absolute_origin)

	const limit_close_distance = 400.0
	
	var distance_to_surface = distance_to_center_origin - radius
	
	# find the ratio for the distance and scale object
	var ratio:float = 1.0
	if distance_to_surface > limit_close_distance:
		var remain = distance_to_surface - limit_close_distance
		remain = sqrt(remain)
		var real_close_distance = limit_close_distance + remain
		ratio = real_close_distance / distance_to_surface
	
	if distance_to_surface <= orbit_height:
		if not orbiting:
			orbiting = true
			print("Enter orbit ", name, " distance = ", distance_to_surface, " ratio = ", ratio)
			target.set_gravity_center(self)
	else:
		if orbiting:
			orbiting = false
			target.remove_gravity_center(self)
	
	# set position ratio and scale
	var vector = (target.position - absolute_origin) * (1.0 - ratio)
	var offset = absolute_origin + vector
	
	position = offset
	
	# set scale
	scale = Vector3(ratio, ratio, ratio)

func update_nearest_point():
	# test : place cursor near
	_nearest_point = position.direction_to(target.position) * radius * 1.1
	cursor.position = _nearest_point
	cursor.look_at(target.position)
