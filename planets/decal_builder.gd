extends Node3D


@export var texture : Texture2D

var next_run : float = 0
var decals = []
const decal_count = 20
var task:int = -1
# Called when the node enters the scene tree for the first time.
func _ready():
	queue_free()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if next_run < Time.get_ticks_msec():
		next_run = Time.get_ticks_msec() + 2000
		if task >= 0:
			if not WorkerThreadPool.is_task_completed(task):
				return
		task = WorkerThreadPool.add_task(init_decals)


func get_planet() -> GeneratedPlanet:
	return get_parent() as GeneratedPlanet
	
	
func init_decals():
	var radius = get_planet().radius
	var mesh = get_planet().mesh_instance.mesh
	
	while decals.size() < decal_count:
		var decal = Decal.new()
		decal.size = Vector3(5, 5, 5)
		decal.texture_albedo = texture
		decal.albedo_mix = 0.5
		decal.upper_fade = 0
		decal.lower_fade = 0
		decals.append(decal)
		get_planet().mesh_instance.add_child(decal)
		
	# create decal
	for decal in decals:
		var index = randi_range(0, mesh.get_faces().size() - 1)
		var face = mesh.get_faces()[index]
		decal.position = face
		decal.posit
		
