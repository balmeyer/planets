extends Node3D

var array : PackedVector3Array
var original_mesh_array: PackedVector3Array
var faces_to_array : Array
var faces_to_center : Array
var current_collision : CollisionShape3D

const max_lod = 6
var t:int = -1

var next_division = 0

# Called when the node enters the scene tree for the first time.
func _ready():
	call_deferred("start_build_surface")

func start_division():
	if t > 0:
		if not WorkerThreadPool.is_task_completed(t):
			return
			
	t = WorkerThreadPool.add_task(thread_division)

func thread_division():
	var point = get_planet().get_nearest_point()
	var f = get_face_from_point(point)
	compute_dots()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if next_division < Time.get_ticks_msec():
		next_division = Time.get_ticks_msec() + 1000
		start_division()

func get_mesh():
	return get_parent().get_mesh()

func get_planet() -> GeneratedPlanet:
	return get_parent() as GeneratedPlanet

func get_height(point:Vector3) -> float:
	return get_parent().get_height(point)

func start_build_surface():
	var task = WorkerThreadPool.add_task(init_surface)

# first init surface
func init_surface():
	print("build surface start")
	array = PackedVector3Array()
	
	var mesh = get_mesh() as Mesh
	var radius = get_planet().radius
	
	# build each face level
	original_mesh_array = PackedVector3Array()
	faces_to_array = []
	faces_to_array.resize(mesh.get_faces().size() / 3)
	faces_to_center = []
	faces_to_center.resize(mesh.get_faces().size() / 3)
	
	for v in mesh.get_faces():
		# var height = get_height(v)
		# v = v.normalized() * radius * (1 + height / 8)
		array.append(v)
		original_mesh_array.append(v)
		
	# set an array of 3 vertices for each face
	var i = 0
	var face_index = 0
	while i < original_mesh_array.size():
		var v1 = original_mesh_array[i]
		var v2 = original_mesh_array[i + 1]
		var v3 = original_mesh_array[i + 2]
		faces_to_array[face_index] = PackedVector3Array()
		faces_to_array[face_index].append_array([v1, v2, v3])
		faces_to_center[face_index] = get_middle_triangle(v1, v2, v3)
		
		face_index += 1
		i = i + 3

	create_surface()
	
# build surface from existing array
func create_surface():
	var radius = get_planet().radius

	var surface = SurfaceTool.new()
	surface.begin(Mesh.PRIMITIVE_TRIANGLES)
	surface.set_color(Color(1, randf(), 0))
	surface.set_uv(Vector2.ZERO)
	
	var vertex = 0
	
	for faces in faces_to_array:
		for v in faces:
			var height = get_height(v)
			var uv = get_parent().get_uv_from_point(v)
			surface.set_uv(uv)
			v = v.normalized() * radius * (1 + height / 8)
			surface.add_vertex(v)
			vertex += 1

	surface.generate_normals()
	
	var array_mesh = ArrayMesh.new()
	surface.commit(array_mesh)
	get_planet().mesh_instance.mesh = array_mesh
	
	if get_planet().orbiting :
		var shape = array_mesh.create_trimesh_shape()
		if current_collision != null:
			current_collision.queue_free()
		current_collision = CollisionShape3D.new()
		current_collision.shape = shape
		get_parent().add_child(current_collision)
	else:
		if current_collision != null:
			current_collision.queue_free()

func get_face_lod(index:int) -> int:
	var a = faces_to_array[index].size()
	if a <= 3:
		return 0
	var r = a / 3
	var lod = 0
	while r > 1:
		r = r / 4
		lod += 1
	return lod

func get_face_from_point(point:Vector3) -> int:
	var face = 0
	var distance = 99999999
	for i in range(faces_to_center.size()):
		var v = faces_to_center[i]
		var d = point.distance_squared_to(v)
		if d < distance:
			face = i
			distance = d
	return face

func compute_dots():
	if get_planet().orbiting:
		pass #return
	var point = get_planet().get_nearest_point() as Vector3
	for i in range(faces_to_center.size()):
		var v = faces_to_center[i] as Vector3
		var d = v.normalized().dot(point.normalized())
		if d > 0.7:
			var lod = ceil( max_lod * d * d)
			# print("face ", i , " dot is ", d, " lod is ", lod)
			# divide_face(i, lod)
			divide_face(i, lod)
		else:
			reset_face(i)
	create_surface()

func increment_face(face_index:int):
	if face_index < 0 or face_index >= faces_to_array.size():
		return
	divide_face(face_index, get_face_lod(face_index) + 1)

func increment_face_until(face_index:int, lod:int):
	if get_face_lod(face_index) < lod:
		increment_face(face_index)

func divide_all_faces(iteration:int = 2):
	for i in range(faces_to_array.size()):
		divide_face(i, iteration)
	
func divide_face(face_index:int, division:int = 1):
	if division > max_lod:
		return
	if face_index < 0 or face_index >= faces_to_array.size():
		return
	var face = faces_to_array[face_index] as PackedVector3Array
	var expected_faces = 3 * pow(4, division)

	if face.size() > expected_faces:
		face = reset_face(face_index)
	
	if face.size() < expected_faces:
		var new_array = PackedVector3Array()
		var i = 0
		while i < face.size():
			var v1 = face[i]
			var v2 = face[i + 1]
			var v3 = face[i + 2]
			new_array.append_array(divide_triangle(v1, v2, v3))
			i = i + 3
		faces_to_array[face_index] = new_array
		divide_face(face_index, division)

func reset_face(index:int) -> PackedVector3Array:
	var pos = index * 3
	var v1 = original_mesh_array[pos]
	var v2 = original_mesh_array[pos + 1]
	var v3 = original_mesh_array[pos + 2]
	faces_to_array[index] = PackedVector3Array()
	faces_to_array[index].append_array([v1, v2, v3])
	return faces_to_array[index]

func divide_triangle(v1 : Vector3, v2 : Vector3, v3 : Vector3) -> PackedVector3Array:
	var result = PackedVector3Array()
	var a1 = get_middle_segment(v1, v2)
	var a2 = get_middle_segment(v1, v3)
	var a3 = get_middle_segment(v2, v3)
	
	result.append_array([v1, a1, a2])
	result.append_array([a1, a3, a2])
	result.append_array([a1, v2, a3])
	result.append_array([a2, a3, v3])

	return result
	
func get_middle_triangle(v1, v2, v3) -> Vector3:
	return get_middle_segment(get_middle_segment(v1, v2), v3)
	
func get_middle_segment(a1:Vector3, a2:Vector3) -> Vector3:
	var dif = (a2 - a1) / 2
	var point = a1 + dif
	return point
	
