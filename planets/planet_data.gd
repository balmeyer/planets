extends Node

class_name PlanetData

var image : Image
var image_texture: Image
var data : PackedByteArray
var _size : Vector2
var noise : FastNoiseLite

func compute_data(size:Vector2, noise: FastNoiseLite):
	_size = size
	self.noise = noise
	image = noise.get_seamless_image(size.x, size.y, true, false)
	data = image.get_data()
	build_texture()

func get_image() -> Image:
	return image_texture
	
func get_height(x, y) -> float:
	var vec = Vector2(x, y).round()
	var index = (vec.y * _size.x) + vec.x
	if index >= data.size():
		return 0
	var value = data[index] as float
	
	value += noise.get_noise_2d(x, y) * 50
	
	value = clamp(value as float / 255.0, 0, 1.0)
	
	if value < 0.2:
		return 0.2
	return value
	
func get_height_from_uv(uv: Vector2) -> float:
	var vec = Vector2(_size.x * uv.x, _size.y * uv.y)
	return get_height(vec.x, vec.y)

func build_texture():
	image_texture = Image.create(_size.x, _size.y, true, Image.FORMAT_RGB8)
	image_texture.fill(Color.BLUE)
	
	var x = 0 
	var y = 0
	for b in data:
		var color = Color.BLUE
		if b > 100:
			color = Color.SADDLE_BROWN
		if b > 150:
			color = Color.SEA_GREEN
		if b > 200:
			color = Color.FLORAL_WHITE
		image_texture.set_pixel(x, y, color)
		x = x + 1
		if x >= _size.x:
			x = 0 
			y = y + 1
	
